
const forecastData = function (defaultCity, cb) {
  let wsUrl = 'ws://' + window.location.host
  if (window.location.host.split(':')[0] === 'localhost') { wsUrl = 'ws://localhost:9000' }

  const ws = new WebSocket(wsUrl)

  const listenCity = function (city) {
    console.log('listen city', city)
    ws.send(JSON.stringify({ city: city }))
  }

  ws.onopen = () => { listenCity(defaultCity) }
  ws.onmessage = message => {
    cb(JSON.parse(message.data))
  }

  // @TODO: socket recconect

  return {
    ws: ws,
    changeCity: listenCity
  }
}

const tubeEl = document.querySelector('.t-tube')
const barEl = document.querySelector('.t-bar')
const forecast = forecastData('moscow', info => {
  const scaleYMax = 0.995; const minC = -53; const maxC = 60
  let scaleY = (info.temp - minC) / (Math.abs(minC) + maxC)
  if (scaleY > scaleYMax) scaleY = scaleYMax

  barEl.style.transform = `scaleY(${scaleY})`

  tubeEl.title = `${info.temp}°C, ${info.temp * 1.8 + 32}°F`
})

document.querySelector('select[name="city"]').addEventListener('change', e => {
  forecast.changeCity(e.target.value)
})
