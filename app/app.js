const port = process.env.PORT || 9000

const http = require('http')
const serveStatic = require('serve-static')
const finalhandler = require('finalhandler')

const WeatherApi = require('./weather.api.js')
const weather = new WeatherApi({ apiKey: process.env.WEATHER_API_KEY })

const servePublicFiles = serveStatic('public/', { index: ['index.html'] })

const server = http.createServer(function onRequest (req, res) {
  servePublicFiles(req, res, finalhandler(req, res))
})
const wss = new (require('ws')).Server({ server })

server.listen(port)

wss.on('connection', client => {
  client.inform = function (data) {
    client.send(JSON.stringify(data))
  }

  client.on('message', rawdata => {
    const data = JSON.parse(rawdata)
    if (!data.city) return

    weather.off('info:' + client.city, client.inform)
    client.city = data.city
    weather.on('info:' + client.city, client.inform)

    // send temperature
    client.inform(weather.cityes[client.city])
  })

  client.on('close', () => {
    weather.off('info:' + client.city, client.inform)
  })
})
