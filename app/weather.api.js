
const EventEmitter = require('events')
const http = require('http')

module.exports = class Weather extends EventEmitter {
  constructor ({ apiKey }) {
    super()

    this.cityes = { moscow: {}, berlin: {}, kair: {}, barrow: {} }

    this.apiKey = apiKey
    this.apiUrl = 'http://api.openweathermap.org/data/2.5/weather?units=metric&appid=' + this.apiKey

    this.updateForecasts()
  }

  updateForecasts () {
    console.log('updating forecasts')
    Object.keys(this.cityes).forEach(async city => {
      const info = await this.getCityForecast(city)
      this.cityes[city] = info
      this.emit('info:' + city, info)
    })

    setTimeout(() => {
      this.updateForecasts()
    }, 5000)
  }

  getCityForecast (city) {
    return new Promise((resolve, reject) => {
      http.get(this.apiUrl + '&q=' + city, responce => {
        let data = ''
        responce.on('data', chunk => { data += chunk })
        responce.on('end', () => resolve((JSON.parse(data)).main))
      }).on('error', reject)
    })
  }
}
